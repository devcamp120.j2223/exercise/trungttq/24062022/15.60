import { Component } from "react";

class UpDownClick extends Component{
    constructor(props){
        super(props)
        this.state = {
            count: 0
        }
    }
    clickUphandler = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    clickDownhandler = () => {
        this.setState({
            count: this.state.count - 1
        })
    }
    render(){
        return(
            <div>
                <button onClick={this.clickUphandler}>Up</button>
                <button onClick={this.clickDownhandler}>Down</button>
                <p>{this.state.count}</p>
            </div>
        )
    }
}
export default UpDownClick;